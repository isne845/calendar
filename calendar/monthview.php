<?php
// Initialize the session
session_start();
require_once "./../login/config.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

$bg = array(
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/120116223_3366385216772758_9054892062919903382_o.jpg?_nc_cat=103&ccb=2&_nc_sid=e3f864&_nc_eui2=AeFFzszrfTBto7pB4j7gZLijGMbA5pp_A3QYxsDmmn8DdKJ7Nx5cQ4k-qcwW6xj1HUBaV1Am3ZKeuOSYVObHPstz&_nc_ohc=sRxOnXQ0PmAAX82wG6-&_nc_ht=scontent-kut2-2.xx&oh=542aa4adb340f27ceb359dabf0a036bf&oe=5FC7014D",
    "https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/84220578_2752649091479710_7230214384935501824_n.jpg?_nc_cat=109&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeHOK2HJNBim4mE8PuqIC0aZATS6y3bv3eYBNLrLdu_d5tMfxa8eN7filKIbRKrmeueo_sYZnQkiZbfV0YyBHw1J&_nc_ohc=pyKSHooSfqAAX-2hOIh&_nc_ht=scontent-kut2-1.xx&oh=d13f0f41bc65c1e88270e6e9cb124d9e&oe=5FC4948E",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/14264250_1095967413814561_8729843890226896238_n.jpg?_nc_cat=103&ccb=2&_nc_sid=19026a&_nc_eui2=AeHIFM52gUU1_PxkxtOOPjd6KfIj9yzxSxEp8iP3LPFLEcplYAuuIAkDYoseDQ5rRttgCdz1DW2gP66iOR9-IW1Z&_nc_ohc=qDUnTxO6yMwAX8wO0Cu&_nc_ht=scontent-kut2-2.xx&oh=0efc77ef9f957e54e4db8da3bcb62ebc&oe=5FC3C351",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/19884473_1416885195056113_1550947311349625551_n.jpg?_nc_cat=104&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFoHh8vzBmChGuCrGcqakbO6PgUgkknVHjo-BSCSSdUeE_9dA97A-fbVxibqIkZ3a-CWyQPzG3M8XMkXYmR7N4O&_nc_ohc=w7KeaCEMdrwAX-quVUm&_nc_ht=scontent-kut2-2.xx&oh=14de3d449313eee7432f31c06305c329&oe=5FC76A03",
    "https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/75210200_2548897005188254_8620222701371392000_n.jpg?_nc_cat=108&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeGG0eu5vdEMhtwaH_1xi_9hpXV_GFdHwQeldX8YV0fBB9H-vpDkZnzppIljXatVeVJEcvyFSWFrBVqCuFbK04RL&_nc_ohc=kLc9ULzSrfQAX9bivFr&_nc_ht=scontent-kut2-1.xx&oh=8eefaed6eb60c7192221f267f21f8378&oe=5FC6790A",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/72412829_2511083038969651_4113698604999245824_n.jpg?_nc_cat=103&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeF38ZrbG2JiGDoc3sUHhnKZeMjwBzNxen54yPAHM3F6fscl7MqXN7JRIyWASVvo-9Z9rEN1CRDdO_GE48dKFxXx&_nc_ohc=vPtcai54i8gAX90ZvMb&_nc_ht=scontent-kut2-2.xx&oh=af7f6dbc5e9d045aac74b75852d7223d&oe=5FC67ECE",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/96804188_2965549160189701_2355373461129396224_n.jpg?_nc_cat=105&ccb=2&_nc_sid=19026a&_nc_eui2=AeH102fbsmeeptt7WxSCWzZTLBbMpIfEe5ssFsykh8R7m9ts4QMltS9t1M38MdEbt-ReQDqHbeAAOVpkH0nNdpDy&_nc_ohc=q__7prv6a8EAX_3P6Yp&_nc_ht=scontent-kut2-2.xx&oh=5490be7ef6390496c17c2b9f7dd6d95e&oe=5FC5E6B3",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/122472175_3453801354697810_1690837178079752561_n.jpg?_nc_cat=101&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFFPYW0Ccj5tbicFbLAg859s5Q2b2CM1WSzlDZvYIzVZMvJlMi_4_fXkFeKXWBxWjKAY2Mhm-sH0Wy7RJEaTAoy&_nc_ohc=NMRNsgM53yQAX9o906J&_nc_ht=scontent-kut2-2.xx&oh=146e7f6d2379b9290d9a39c0b817217e&oe=5FC407DE",
    "https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/118586737_3280452218699392_1410209582401470822_n.jpg?_nc_cat=110&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeHL0QeA3gbHtiXRWpUXUGJkeGwL7yZy8-p4bAvvJnLz6mus0K642c6dYVhBplDf4siWm4C65Xa4OhAttJ7HMtnT&_nc_ohc=dQMFgmvrO4IAX_QQHy8&_nc_ht=scontent-kut2-1.xx&oh=7f4be2711a21b9b54e8bdc4b167eb55b&oe=5FC4B613",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/22814271_1519591328118832_7847332838337306225_n.jpg?_nc_cat=107&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFJzTSMTBYrkXFPcePzirh9GClVNFROLF8YKVU0VE4sX4slSiGWQZJ2YV7PfJAkWIgSknPfGVt5ArOk7SlLiOI_&_nc_ohc=zWtBkv-mSs8AX_kdGB9&_nc_ht=scontent-kut2-2.xx&oh=11d6bad7bbc3f6e9f674f4a97a58f882&oe=5FC73346",
    "https://scontent-kut2-2.xx.fbcdn.net/v/t1.0-9/14522817_1121574707920498_4933255831829963233_n.jpg?_nc_cat=105&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeG0JB9J6D4Gn-loGcd0ww8ovFDQ3lSpmSu8UNDeVKmZK_UQ989uYkT0RCRRb9LVCN2gjFeprJ5UvPEwTdzmqMlp&_nc_ohc=7ZDKt_-5TrMAX-imHbX&_nc_ht=scontent-kut2-2.xx&oh=aef6185df0bcc4e178fa53f6594d4174&oe=5FC4E431",
    "https://scontent-kut2-1.xx.fbcdn.net/v/t1.0-9/68715193_2396362047108418_176485093865422848_n.jpg?_nc_cat=100&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeFbpueU9_yqU40JQLXEYCq1oWUFZOAA822hZQVk4ADzbXorRQbw5xjYzLNAqkOrAUtMdXG3vNUfdZxxYfAC-NEw&_nc_ohc=H4i9pax1O-oAX--vyfH&_nc_ht=scontent-kut2-1.xx&oh=0b4c876c1348508ac3395f4e34199908&oe=5FC4851A",
)
?>

<?php
if(isset($_GET['date'])){
    $day = date('d', strtotime($_GET['date'])); //Gets day of appointment (1‐31)
    $month = date('m', strtotime($_GET['date'])); //Gets month of appointment (1‐12)
    $year = date('Y', strtotime($_GET['date'])); //Gets year of appointment (e.g. 2016)
    $days = date('t', strtotime($_GET['date'])); //Gets number of days in month
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); //Gets the day of the week for the 1st of
    //the month. (e.g. 0 for Sun, 1 for Mon)
    $nmonth = strtotime($_GET['date']);
}else{
    $day = date('d', strtotime(date("Y-m-d"))); //Gets day of appointment (1‐31)
    $month = date('m', strtotime(date("Y-m-d"))); //Gets month of appointment (1‐12)
    $year = date('Y', strtotime(date("Y-m-d"))); //Gets year of appointment (e.g. 2016)
    $days = date('t', strtotime(date("Y-m-d"))); //Gets number of days in month
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); //Gets the day of the week for the 1st of
    //the month. (e.g. 0 for Sun, 1 for Mon)
    $nmonth = strtotime(date("Y-m-d"));
}
    $today = date('d'); //Gets today’s date
    $todaymonth = date('m'); //Gets today’s month
    $todayyear = date('Y'); //Gets today’s year
    $userID = $_SESSION['id'];
    $sql = "SELECT Appointment,StartTime,EndTime,AppointmentID,Date FROM appointment WHERE UserID = '$userID' "; //sql command get data
    $monthName = date("F", mktime(null, null, null, $month)); //change number to name month
	$nextMonth = date('Y-m-d',strtotime('+1 month', $nmonth));
    $prevMonth = date('Y-m-d',strtotime('-1 month', $nmonth));
    $app_list = array();
    $st_list = array();
    $et_list = array();
    $appid_list = array();
    $dt_list = array();

    $app = mysqli_query($link, $sql); //make sql command active (Connect with database)

    while($row = mysqli_fetch_assoc($app)) { //access to each appointment
        array_push($app_list, $row['Appointment']);
        array_push($st_list, $row['StartTime']);
        array_push($et_list, $row['EndTime']);
        array_push($appid_list, $row['AppointmentID']);
        array_push($dt_list, $row['Date']);
    }
?>

<html>
<head>
<link href='https://fonts.googleapis.com/css?family=Acme' rel='stylesheet'>
<script src="alert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="alert/dist/sweetalert.css">
 <!-- Compiled and minified CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<style>

body{
    
    background-image: url("<?php echo $bg[$month-1];?>");
    background-image:url(bg2.jpg);
        background-repeat: no-repeat;
        background-position: center;
        
}

/* width */
::-webkit-scrollbar {
  width: 3px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
input{
    width: 80% !important;
}
    .calendar{
        
        position:relative;
        width: 720px;
     }
    div.date, div.days{
        background-color:white;
        font-family: 'Acme';font-size: 18px;
        width: 90px;
        border: 1px solid black;
        float: left;
        margin: 1px;
    }
    .blankday{
        background: linear-gradient(90deg,#ffdfba,#bae1ff);
    }
    div.date{
        height: 90px;
        overflow: auto;
    }
    .today{
        background:#cfc;
    }
    #form{
        font-family: 'Acme';font-size: 18px;
        position: absolute;
        left:55%;
    }
    #box{
        position: absolute;
        width: 30%;
        height: 41%;
        background: pink;
       border-radius: 50px;
        right: 10;
        top: 30%;
        padding: 10px;
    }
    #delete{
        position:center;
        padding-left: 110px;
    padding-right: 30px;
    }
    .delete2{
        font-family: 'Acme';font-size: 18px;
        background-color:#CFA1BE;
        color:#5F2081;
        border-radius: 50px;
    }
    
    
</style>
</head>


<body>
<div class="page-header">
        
    <!-- Month Bar -->
        <a href="?date=<?php echo $prevMonth;?>"><i class="material-icons">navigate_before</i></a>
        <?php
        echo("$monthName"." ");
        echo("$year");
        ?>
        <a href="?date=<?php echo $nextMonth;?>"><i class="material-icons">navigate_next</i></a>
        <a href="./../login/logout.php" class="black waves-effect waves-light btn">
            logout
        </a>
        <a href="monthview.php" class="blue waves-effect waves-light btn"> Today </a>
        <a href="#" class="blue waves-effect waves-light btn"> Month View </a>
        <a href="weekview.php" class="blue waves-effect waves-light btn"> Week View </a>
        <a href="dayview.php" class="blue waves-effect waves-light btn"> Day View </a>

    

    <div class="calendar" >
        <div class="days">Sunday</div>
        <div class="days">Monday</div>
        <div class="days">Tuesday</div>
        <div class="days">Wednesday</div>
        <div class="days">Thursday</div>
        <div class="days">Friday</div>
        <div class="days">Saturday</div>

        <?php
            for($i=1; $i<=$firstday; $i++) //show blank day before 1st day
            {
                echo '<div class="date blankday"></div>';
            }
        ?>
        <?php
            for($i=1; $i<=$days; $i++) //show day in month
            {  
                if($i<10){
                    $app_date = $year."-".$month."-0".$i;
                }else{
                    $app_date = $year."-".$month."-".$i;
                }
                

                echo '<div class="date';
                if ($today == $i && $todaymonth==$month && $todayyear == $year)
                {
                    echo ' today';
                }
                echo '">' . $i . '<br>';


                for($j=0;$j<count($app_list);$j++){
                    if($dt_list[$j] == $app_date){ //check if the day is match
                        $hold_s = "'".$st_list[$j]."'";
                        $hold_e = "'".$et_list[$j]."'";
                        $hold_d = "'".$dt_list[$j]."'";
                        $hold_t = "'".$app_list[$j]."'";

                        echo '<div onclick="monthAppointment('. $hold_t.','.$hold_s.','.$hold_e.','.$appid_list[$j].','.$hold_d.')">'.$app_list[$j].'</div>';
                    }
                }
                

                echo '</div>';
            } 
     ?>

        <?php
            $daysleft = 7-(($days + $firstday)%7); 
            if($daysleft<7)
            {
                for($i=1; $i<=$daysleft; $i++) //show blank day after last day of month
                {
                        echo '<div class="date blankday"></div>';
                }
            }
        ?>


    </div>
    
        <div id="form">
    <form action="add_appointment.php" method="post" >
            <input name="Date" type="date"><br>
            From: <input name="StartTime" type="time"><br>
            To: <input name="EndTime" type="time"><br>
            <input name="Assi" type="text" placeholder="Enter Title"><br>
            <div id="button">
            <input type="submit" class="delete2" value="Submit"> 
        </div>
    </form>
    </div>

    <div id="box">
        <form action = "./delete_appointment.php" method="post">
        <input name="appy" type="number" id="appy" hidden>
        <input name="Date" type="date" id="appyDate" hidden> 
        <div id="delete" >
        <input value="Delete" type="submit" class="delete2">
        </div>
        </form>
       
    </div>
    <img src="gura2.gif">
</body>



<?php
mysqli_close($link);
?>
<script>
//show in the blue box
function monthAppointment(Appointment,StartTime,EndTime,AppointmentID,Date){
 document.getElementById("box").innerHTML += Appointment+"<br/>"
 document.getElementById("box").innerHTML += StartTime+"<br/>"
 document.getElementById("box").innerHTML += EndTime+"<br/>"
 document.getElementById("box").innerHTML += Date+"<br/>"
 document.getElementById("appy").value = AppointmentID;
 document.getElementById("appyDate").value = Date;
}
</script>