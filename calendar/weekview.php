<?php
// Initialize the session
session_start();
require_once "./../login/config.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

?>

<?php
if(isset($_GET['date'])){
    $day = date('d', strtotime($_GET['date'])); //Gets day of appointment (1‐31)
    $month = date('m', strtotime($_GET['date'])); //Gets month of appointment (1‐12)
    $year = date('Y', strtotime($_GET['date'])); //Gets year of appointment (e.g. 2016)
    $days = date('t', strtotime($_GET['date'])); //Gets number of days in month
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); //Gets the day of the week for the 1st of
    //the month. (e.g. 0 for Sun, 1 for Mon)
    $nmonth = strtotime($_GET['date']);
}else{
    $day = date('d', strtotime(date("Y-m-d"))); //Gets day of appointment (1‐31)
    $month = date('m', strtotime(date("Y-m-d"))); //Gets month of appointment (1‐12)
    $year = date('Y', strtotime(date("Y-m-d"))); //Gets year of appointment (e.g. 2016)
    $days = date('t', strtotime(date("Y-m-d"))); //Gets number of days in month
    $firstday = date('w', strtotime('01-'.$month.'-'.$year)); //Gets the day of the week for the 1st of
    //the month. (e.g. 0 for Sun, 1 for Mon)
    $nmonth = strtotime(date("Y-m-d"));
}
    $longdate = $year."-".$month."-".$day;
    $today = date('d'); //Gets today’s date
    $todaymonth = date('m'); //Gets today’s month
    $todayyear = date('Y'); //Gets today’s year
    $userID = $_SESSION['id'];
    $monthName = date("F", mktime(null, null, null, $month)); //change number to name month
    $dayname = date('D', strtotime($longdate));

    $minus = $day+1;
    $plus = $days-$day+1;

    $firstday_week=array();
    for($i=1; $i<=$days;$i++){
        if($i%7==1){
            array_push($firstday_week,$i);
        }
    }
    
    $weekindex=0; 
    for($i=0;$i<sizeof($firstday_week);$i++){
        if($day>=$firstday_week[$i]){
            $weekindex=$i;
        }
    }
    $day=$firstday_week[$weekindex];

    if($weekindex<sizeof($firstday_week)-1){
        $next_week = date("Y-m-d",strtotime("+7 day", $nmonth ));             
    }else{
        $next_week = date("Y-m-d",strtotime("+$plus day", $nmonth ));
    }

    if($weekindex==0){
        $prev_week = date("Y-m-d",strtotime("-$minus day",  $nmonth));
    }else{
        $prev_week = date("Y-m-d",strtotime("-7 day",  $nmonth));
    }
?>

<html>

<head>
    <script src="alert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="alert/dist/sweetalert.css">
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <style>
    /* width */
    ::-webkit-scrollbar {
        width: 3px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
        background: #f1f1f1;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
        background: #888;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
        background: #555;
    }

    input {
        width: 15% !important;
    }

    .calendar {
        position: relative;
        width: 720px;
        display: flex;
        margin-top: 3%;
    }

    div.date,
    div.days {
        width: 90px;
        border: 1px solid black;
        float: left;
        margin: 1px;
    }

    .blankday {
        background: linear-gradient(90deg, #ffb3ba, #ffdfba, #ffffba, #baffc9, #bae1ff);
    }

    div.date {
        height: 90px;
        overflow: auto;
    }

    .today {
        background: #cfc;
    }

    #form {
        position: absolute;
        top: 8%;
        left: 7%;
        width: 80%;
    }

    #showapp {
        margin-left: 12%;
    }

    .time-list {
        padding: 2px;
        text-align: center;
        font-size: 13px;
        width: 6vw;
        margin-top: -2%;
    }

    .app {
        width: 45% !important;
    }

    .appcon {
        display: flex;
        background-color: DodgerBlue;
    }

    .apmbox {
        background-color: #f1f1f1;
        margin: 3px;
        padding: 15px;
        font-size: 15px;
        width: 162px;
    }

    .content{
        z-index: 20;
    }
    </style>
</head>


<body>
    <div class="bg"></div>
    <div class="content">
        <div class="page-header">

            <a href="?date=<?php echo $prev_week;?>"><i class="material-icons">navigate_before</i></a>
            <?php
            echo("$monthName"." ");
            echo("$year"." ");
      
        ?>
            <a href="?date=<?php echo $next_week;?>"><i class="material-icons">navigate_next</i></a>
            <a href="./../login/logout.php" class="black waves-effect waves-light btn">
                logout
            </a>
            <a href="weekview.php" class="blue waves-effect waves-light btn"> Today</a>
            <a href="monthview.php" class="blue waves-effect waves-light btn"> Month View </a>
            <a href="#" class="blue waves-effect waves-light btn"> Week View </a>
            <a href="dayview.php" class="blue waves-effect waves-light btn"> Day View </a>
        </div>



        <div class="calendar">
            <div id="list-example" class="list-group" style="position: fixed;">
                <?php
            for($i = 0; $i < 24 ; $i++){
                $time;
                if($i<10){
                    $time = "0".$i.":00";
                }else{
                    $time = $i.":00";
                }

                echo '<a class="time-list list-group-item list-group-item-action" href="#list-item-'.$i.'">'.$time.'</a>'; //slot time
            }
        
        ?>
            </div>
            <div data-spy="scroll" data-target="#list-example" data-offset="0" class="scrollspy-example" id="showapp">
                <?php

                for($i = 0; $i < 24 ; $i++){
                    echo '<br>';
                    $time;
                    if($i<10){
                        $time = "0".$i.":00";
                    }else{
                        $time = $i.":00";
                    }
                    echo '<h5 id="list-item-'.$i.'">'.$time.'</h5>'; //displayed time
                    echo '<div class = "appcon">';

                    $numdate = $day;
                    for($k = 0; $k < 7; $k++){
                        if($numdate <= $days){
                            $weekdate =  $year."-".$month."-".$numdate;
                            //get apm of each day in week
                            $sql = "SELECT Appointment,StartTime,EndTime,AppointmentID FROM appointment WHERE UserID = '$userID' AND Date='$weekdate'"; //sql command get data
                            $app_list = array();
                            $st_list = array();
                            $et_list = array();
                            $appid_list = array();
                            $app = mysqli_query($link, $sql); //make sql command active
                            while($row = mysqli_fetch_assoc($app)) { //access to each appointment
                                array_push($app_list, $row['Appointment']);
                                array_push($st_list, $row['StartTime']);
                                array_push($et_list, $row['EndTime']);
                                array_push($appid_list, $row['AppointmentID']);
                            }

                            echo '<div class = "apmbox">'; //apm box 

                            for($j = 0; $j <sizeof($st_list); $j++){ //show apm
                                if( $i ==  date("H", strtotime($st_list[$j])) ){
                                    echo ' 
                                    <div title="'.$st_list[$j].'-'.$et_list[$j].'" style = "margin-top: 5px;">
                                            <b>'.$app_list[$j].'</b> 
            
                                            <form action = "./delete_app_weekview.php" method="post">                           
                                                <input name="appy" type="number" value="'.$appid_list[$j].'" hidden>
                                                <input name="Date" type="date" value="'.$longdate.'" hidden> 
                                                <input value="Delete" type="submit"  class="app">
                                            </form>
                                  </div>';
                                }
                            }

                            echo '</div>';
                            
                            $numdate++;
                        }      
                    } 

                    echo "</div>";
                    
                }  
        
            ?>
            </div>
        </div>

        <div id="form">
            <form action="add_app_weekview.php" method="post">
                <input name="Date" type="date">
                From: <input name="StartTime" type="time">
                To: <input name="EndTime" type="time">
                <input name="Assi" type="text" placeholder="Enter Title">
                <input type="submit" value="Submit">
            </form>
        </div>
    </div>
</body>



<?php
mysqli_close($link);
?>

<script>
function monthAppointment(Appointment, StartTime, EndTime, AppointmentID, Date) {
    document.getElementById("box").innerHTML += Appointment + "<br/>"
    document.getElementById("box").innerHTML += StartTime + "<br/>"
    document.getElementById("box").innerHTML += EndTime + "<br/>"
    document.getElementById("box").innerHTML += Date + "<br/>"
    document.getElementById("appy").value = AppointmentID;
    document.getElementById("appyDate").value = Date;
}
</script>